package ch.psi.ftpp.experimental.meshgeneration;

import vtk.vtkPolyData;
import vtk.vtkImageData;
import vtk.vtkPolyDataToImageStencil;
import vtk.vtkImageStencil;
import vtk.vtkMetaImageReader;
import vtk.vtkMetaImageWriter;
import vtk.vtkPolyDataReader;
import java.io.File;

public class MeshToBinaryImage {	
	
	public static void main(String[] args) {

		if (args.length != 3) {
			System.out.println("Error in Mesh to binary image conversion!");
			System.out.println(
					"Arguments: inputPathVtkMesh inputPathReferenceMhaFile outputPathBinaryMhaImage");
		} else {
			convertMeshToBinaryImage(args[0],args[1],args[2]);
		}
			
	} 
			
public static void convertMeshToBinaryImage(String lungMesh, String referenceMHA, String outputLungMask) {		
			
		// check if files exist:
		File tempFileMesh = new File(lungMesh);
		boolean exists2 = tempFileMesh.exists();			
		if(exists2== false) {
			System.out.println("ERROR: File " + lungMesh + " does not exist! ");
			System.exit(0);
		}
		File tempFileReference = new File(referenceMHA);
		boolean exists1 = tempFileReference.exists();
		if(exists1== false) {
			System.out.println("ERROR: File " + referenceMHA + " does not exist! ");
			System.exit(0);
		}

		
		// read smoothed mask file to get output header
		vtkMetaImageReader mhaReader = new vtkMetaImageReader();
		mhaReader.SetFileName(referenceMHA);
		mhaReader.Update();
		System.out.println("Reading " + referenceMHA);

		
		// read mesh
		vtkPolyDataReader vtkReader = new vtkPolyDataReader();
		vtkReader.SetFileName(lungMesh);
		vtkReader.Update();
		System.out.println("Reading " + lungMesh);
		
		vtkPolyData pd = vtkReader.GetOutput();
	
		//create empty image with same header as smooth mask
		vtkImageData whiteImageFull = new vtkImageData();
		whiteImageFull.SetSpacing(mhaReader.GetOutput().GetSpacing());
		whiteImageFull.SetDimensions(mhaReader.GetOutput().GetDimensions());
		whiteImageFull.SetExtent(mhaReader.GetOutput().GetExtent());
		whiteImageFull.SetOrigin(mhaReader.GetOutput().GetOrigin());
		whiteImageFull.AllocateScalars(mhaReader.GetOutput().GetScalarType(),1);

		// fill the image with foreground voxels:
		char inval = 255;
		char outval = 0;
		int count = whiteImageFull.GetNumberOfPoints();

		for (int i = 0; i < count; ++i) {
			whiteImageFull.GetPointData().GetScalars().SetTuple1(i, inval);
		}
	
		
		// polygonal data --> image stencil
		vtkPolyDataToImageStencil pol2stenc = new   vtkPolyDataToImageStencil() ;
		pol2stenc.SetInputData(pd);
		pol2stenc.SetOutputOrigin(mhaReader.GetOutput().GetOrigin());
		pol2stenc.SetOutputSpacing(mhaReader.GetOutput().GetSpacing());
		pol2stenc.SetOutputWholeExtent(whiteImageFull.GetExtent());
		pol2stenc.Update();
				
		// cut the corresponding white image and set the background
		vtkImageStencil imgstenc =  new  vtkImageStencil();
		imgstenc.SetInputData(whiteImageFull);
		imgstenc.SetStencilConnection(pol2stenc.GetOutputPort());
		imgstenc.ReverseStencilOff();
		imgstenc.SetBackgroundValue(outval);
		imgstenc.Update();
		
		System.out.println("Converted to binary file ");
		
		// write output binary mask
		vtkMetaImageWriter writer =	 new   vtkMetaImageWriter() ;
		writer.SetFileName(outputLungMask);
		writer.SetInputConnection(imgstenc.GetOutputPort());
		writer.Write();
		System.out.println("Saved converted output: " + outputLungMask);

	}

}

