package ch.psi.ftpp.experimental.meshgeneration;

import vtk.vtkMarchingCubes;
import vtk.vtkMetaImageReader;
import vtk.vtkPolyDataWriter;
import vtk.vtkImageResample;
import vtk.vtkMetaImageWriter;
import vtk.vtkImageGaussianSmooth;
import vtk.vtkQuadricDecimation;
import vtk.vtkImageThreshold;
import java.io.File;

public class BinaryImageToMesh {
	public static void main(String[] args) {

		if (args.length != 2) {
			System.out.println("Error in Mesh to binary image conversion!");
			System.out.println(
					"Arguments: inputPathMhaMask outputPathVtkMesh");
		} else {
			convertBinaryImageToMesh(args[0],args[1]);
		}
			
	} 

	public static void convertBinaryImageToMesh(String inputMask, String outputMesh) {		

		// parameter for resampling and mesh reduction
		float desiredMhaSpacing = 5; // in mm (same for x,y,z)
		double meshReduction = 0.90; // Percentage of points to remove from mesh
		
		// check if files exist:
		File fileInputMask = new File(inputMask);
		boolean exists1 = fileInputMask.exists();
		if(exists1== false) {
			System.out.println("ERROR: File " + inputMask + " does not exist! ");
			System.exit(0);
		}

		vtkMetaImageReader mhaReader = new vtkMetaImageReader();
		mhaReader.SetFileName(inputMask);
		mhaReader.Update();
		System.out.println("Read input file " + inputMask);

		// resample mha mask to 5mm voxels --> not binary (0,255) anymore!
		double[] spacing = mhaReader.GetOutput().GetSpacing();
		System.out.println("Original mha spacing [mm] " + spacing[0] + " " + spacing[1] + " " + spacing[2]);

		vtkImageResample imageResampler = new vtkImageResample();
		imageResampler.SetInputData(mhaReader.GetOutput());
		imageResampler.SetInterpolationModeToCubic();
		imageResampler.SetAxisMagnificationFactor(0, spacing[0] / desiredMhaSpacing);
		imageResampler.SetAxisMagnificationFactor(1, spacing[1] / desiredMhaSpacing);
		imageResampler.SetAxisMagnificationFactor(2, spacing[2] / desiredMhaSpacing);
		imageResampler.Update();

		double[] spacingResampled = (imageResampler.GetOutput()).GetSpacing();
		System.out.println("Resampled mha spacing [mm] " + spacingResampled[0] + " " + spacingResampled[1] + " "
				+ spacingResampled[2]);

		// smooth image
		vtkImageGaussianSmooth gaussianImageSmoothening = new vtkImageGaussianSmooth();
		gaussianImageSmoothening.SetDimensionality(3);
		gaussianImageSmoothening.SetStandardDeviation(1); // pixel units
		gaussianImageSmoothening.SetRadiusFactor(1);
		gaussianImageSmoothening.SetInputConnection(imageResampler.GetOutputPort());
		gaussianImageSmoothening.Update();

		//convert to binary image (threshold at 255/2=127.5)			
		vtkImageThreshold thresholdImage = new vtkImageThreshold();
		thresholdImage.SetInputConnection(gaussianImageSmoothening.GetOutputPort());
		thresholdImage.ThresholdBetween(127.5, 255);
		thresholdImage.ReplaceInOn();
		thresholdImage.SetInValue(255);
		thresholdImage.ReplaceOutOn();
		thresholdImage.SetOutValue(0);
		thresholdImage.Update();
		
		
		// save resampled mha
		String smoothedMask = inputMask.substring(0, inputMask.length()-4) + "_smooth.mha";
		vtkMetaImageWriter mhaWriter = new vtkMetaImageWriter();
		mhaWriter.SetFileName(smoothedMask);
		mhaWriter.SetInputConnection(thresholdImage.GetOutputPort());
		mhaWriter.Write();
		System.out.println("Saved smoothed mask: " + smoothedMask);

		// isosurface with marching cubes algorithm resampled
		vtkMarchingCubes contourExtractor = new vtkMarchingCubes();
		contourExtractor.ComputeScalarsOff();
		contourExtractor.ComputeGradientsOff();
		contourExtractor.ComputeNormalsOff();
		contourExtractor.SetInputConnection(gaussianImageSmoothening.GetOutputPort());
		contourExtractor.SetValue(0, 127);
		contourExtractor.Update();

		// reduce number of mesh points
		vtkQuadricDecimation reduceTraingles = new vtkQuadricDecimation();
		reduceTraingles.SetInputConnection(contourExtractor.GetOutputPort());
		reduceTraingles.SetTargetReduction(meshReduction);
		reduceTraingles.Update();

		vtkPolyDataWriter vtkWriterResampledDecimated = new vtkPolyDataWriter();
		vtkWriterResampledDecimated.SetFileName(outputMesh);
		vtkWriterResampledDecimated.SetInputConnection(reduceTraingles.GetOutputPort());
		vtkWriterResampledDecimated.Write();

		System.out.println("Saved mesh: " + outputMesh);
	}
}
