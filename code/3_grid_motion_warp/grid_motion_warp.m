clear all; clc; close all;
addpath('../matlab-scripts');
addpath('functions');


%Script for 4DCT(MRI) generation
%Created by Alisha Duetschler August 2019
%Adapted in June 2021

%Prequisites: 
%   -Corresponding mesh of surface of lungs for CT and MRI
%   -NonmovingMask_Body  with zero values outside of the ribcage
%   -reference CT
%   -moving MR lung meshes

%   1) Insert corresponding grid points inside CT meshes (insertGridPointsInsideCTmesh.m)
%   2) Extract motion vectors from moving MRI mesh points and apply to CT mesh
%      (applyMotionToCTmesh.m)
%   3) Warp reference CT to different phases (create_simulated_vectorfield)

%% 

%% input that has to be changed
CTNo = 1;
MRINo = 1;

CTbasepath = '../../example/data/CT/CT';
MRIbasepath = '../../example/data/MRI/MRI';
CT_MR_basepath = '../../example/results/';


reference_phase = 24; %here:end exhale
start_phase = 24; 
end_phase = 45; %two cycles in this example

%% define paths
CT_path = [CTbasepath num2str(CTNo) '/']
MR_path = [MRIbasepath num2str(MRINo) '/']
CT=[CT_path 'CT.mha'];

nonMoving_mask= [CT_path 'structures/NonmovingMask_Body.mha'];

CT_MR_path = [CT_MR_basepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/']
CT_mesh_basename = [CT_MR_path 'deformed_lungs_']
MovingMR_mesh_path = [MR_path 'MovingMRmesh/']

%% set up directories
str = ['mkdir -p ' CT_MR_path];
system(str);

str=['mkdir -p ' CT_MR_path 'MotionVectors/'];
system(str);

str=['mkdir -p ' CT_MR_path 'MovingCT/'];
system(str);

MovingCT_mesh_basepath = [CT_MR_path '/MovingCTmesh/'];
str=['mkdir -p ' MovingCT_mesh_basepath];
system(str);

%% for both sides separately first insert points inside of the mesh and then apply the DFVs to the meshes
for sides = 0:1

    if sides==0
        side = 'Left'
    else
        side = 'Right'
    end
    
    %make directories for moving meshes
    
    str=['mkdir -p ' MovingCT_mesh_basepath side '/'];
    system(str);

    %%
    disp('---------------------------------------------------');
    disp(' Insert grid points inside CT meshes');
    % based on ray casting
    insertGridPointsInsideCTmesh(CT_mesh_basename, MovingMR_mesh_path, MovingCT_mesh_basepath, side);
 

    %%
    disp('---------------------------------------------------');


    disp(' Extract motion vectors at MRI mesh points and apply to CT mesh');

    for phase=start_phase:end_phase
        applyMotionToCTmesh(MovingMR_mesh_path, MovingCT_mesh_basepath, side, phase);
    end

    disp('---------------------------------------------------');

end
%% execute create_simulated_vectorfield_bothSides with the appropriate inputs

%read dimensions of CT from header (you might have to decrease the min
%value slightly or increase the max value slightly if an error occurs. This
%could be, if the lung is very close to the edge of the CT and the
%defromed lung mesh extends outside for some states)
CT_header = mha_read_header(CT);
xmin=0;
xmax=CT_header.Dimensions(1)-1;
ymin=0;
ymax=CT_header.Dimensions(2)-1;
zmin=0;
zmax=CT_header.Dimensions(3)-1;

disp(' Warp reference CT to different phases');
 
command1 = ['ccode/build/create_simulated_vectorfield_bothSides 1 ' CT_MR_path ' ' num2str(reference_phase) ' ' num2str(start_phase) ' ' num2str(end_phase) ' ' CT ' ' CT_MR_path  'MotionVectors/motionfield ' CT_MR_path 'MovingCT/state ' num2str(xmin) ' ' num2str(xmax) ' ' num2str(ymin) ' ' num2str(ymax) ' ' num2str(zmin) ' ' num2str(zmax) ' ' nonMoving_mask];
command2 = ['ccode/build/create_simulated_vectorfield_bothSides 0 ' CT_MR_path ' ' num2str(reference_phase) ' ' num2str(start_phase) ' ' num2str(end_phase) ' ' CT ' ' CT_MR_path  'MotionVectors/deformationfield ' CT_MR_path 'MovingCT/state ' num2str(xmin) ' ' num2str(xmax) ' ' num2str(ymin) ' ' num2str(ymax) ' ' num2str(zmin) ' ' num2str(zmax) ' ' nonMoving_mask];
command=[command1 ' & ' command2 '\n'];

ccFile = [CT_MR_path 'MovingCT/create_simulated_vectorfield.txt'];        
fileID = fopen(ccFile, 'w');
fprintf(fileID,'module load gcc/7.4.0\n');
fprintf(fileID,command);

fclose(fileID);

system(['chmod +x ' ccFile]);
system(ccFile);

disp('---------------------------------------------------');



