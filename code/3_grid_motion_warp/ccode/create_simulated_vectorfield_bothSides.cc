#include <vtkSmartPointer.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkDelaunay3D.h>
#include <vtkUnstructuredGrid.h>
#include <vtkTetra.h>

#include <itkImage.h>
#include <itkVector.h>
#include <itkImageFileReader.h>
#include <itkImageFileWriter.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkWarpImageFilter.h>

#include <cstdlib>
#include <cstdio>


#include <itkPointSet.h>
#include <itkBSplineScatteredDataPointSetToImageFilter.h>



typedef itk::Vector<double, 3> VectorType;
typedef itk::Image<VectorType, 3> VectorImageType;
typedef itk::Image<short, 3> ImageType;
typedef itk::Image<unsigned char, 3> ImageMaskType;
typedef itk::ImageFileWriter<VectorImageType> VectorImageWriterType;
typedef itk::ImageFileReader<ImageType> ImageReaderType;
typedef itk::ImageFileWriter<ImageType> ImageWriterType;
typedef itk::ImageFileReader<ImageMaskType> ImageMaskReaderType;

typedef itk::WarpImageFilter<ImageType, ImageType, VectorImageType >  WarperType;
typedef itk::LinearInterpolateImageFunction<ImageType, double >  InterpolatorType;


typedef itk::PointSet <VectorType, 3> PointSetType;
typedef itk::BSplineScatteredDataPointSetToImageFilter < PointSetType , VectorImageType > FilterType ;

using namespace std;

void printInfo(){
	cout << endl << "Creates B-spline interpolated vector field from fixed.vtk + moving.vtk + refimage.mha + roi extent (in slice numbers)" << endl;
	cout << "Usage: create_simulated_vectorfield_bothSides vf/def [0/1] stepsvtkprefix refstep startstep stopstep refimage.mha outputprefixVF outputprefixWarpedImage xmin xmax ymin ymax zmin zmax nonmovingmask.mha" << endl << endl;
}

int main (int argc, char** argv){
	int vectorfield_or_deffield,refstep,startstep,stopstep;
	string stepsvtkprefix,fixedvtkfilename_l,fixedvtkfilename_r,movingvtkfilename_l,movingvtkfilename_r,reffilename,outfilename,outputprefixVF,outputprefixWarpedImage;
	string nonmovingpointsfilename;

	if (argc==1 || (argc<15)){
		printInfo();
		exit(1);
	}

	vectorfield_or_deffield=atoi(argv[1]);
	stepsvtkprefix=argv[2];
	refstep=atoi(argv[3]);
	startstep=atoi(argv[4]);
	stopstep=atoi(argv[5]);


	reffilename=argv[6];
	outputprefixVF=argv[7];
    outputprefixWarpedImage=argv[8];
	int *extent=new int[6];
	for (int i=0;i<6;++i)
	{
	  extent[i]=atoi(argv[9+i]);
	}

	
	if (argc==16)
	{
	  nonmovingpointsfilename=argv[15];
	}

	cout << "Loading .mha reference file..." << endl;

	ImageReaderType::Pointer itkreader=ImageReaderType::New();
	itkreader->SetFileName(reffilename.c_str());
	itkreader->Update();


	for (int step=startstep;step<=stopstep;step+=1)
	{
	  if (vectorfield_or_deffield==1)
	  {
	    char buffer_l[255];
	    sprintf(buffer_l,"%sMovingCTmesh/Left/state_%04d.vtk",stepsvtkprefix.c_str(),refstep);
	    movingvtkfilename_l="";
	    movingvtkfilename_l.append(buffer_l);
        
	    char buffer_l2[255];
	    sprintf(buffer_l2,"%sMovingCTmesh/Left/state_%04d.vtk",stepsvtkprefix.c_str(),step);
	    fixedvtkfilename_l="";
	    fixedvtkfilename_l.append(buffer_l2);
        
        char buffer_r[255];
        sprintf(buffer_r,"%sMovingCTmesh/Right/state_%04d.vtk",stepsvtkprefix.c_str(),refstep);
        movingvtkfilename_r="";
        movingvtkfilename_r.append(buffer_r);
        
        char buffer_r2[255];
        sprintf(buffer_r2,"%sMovingCTmesh/Right/state_%04d.vtk",stepsvtkprefix.c_str(),step);
        fixedvtkfilename_r="";
        fixedvtkfilename_r.append(buffer_r2);

	  }
	  else
	  {
        char buffer_l[255];
        sprintf(buffer_l,"%sMovingCTmesh/Left/state_%04d.vtk",stepsvtkprefix.c_str(),step);
        movingvtkfilename_l="";
        movingvtkfilename_l.append(buffer_l);
        
        char buffer_l2[255];
        sprintf(buffer_l2,"%sMovingCTmesh/Left/state_%04d.vtk",stepsvtkprefix.c_str(),refstep);
        fixedvtkfilename_l="";
        fixedvtkfilename_l.append(buffer_l2);
        
        char buffer_r[255];
        sprintf(buffer_r,"%sMovingCTmesh/Right/state_%04d.vtk",stepsvtkprefix.c_str(),step);
        movingvtkfilename_r="";
        movingvtkfilename_r.append(buffer_r);
        
        char buffer_r2[255];
        sprintf(buffer_r2,"%sMovingCTmesh/Right/state_%04d.vtk",stepsvtkprefix.c_str(),refstep);
        fixedvtkfilename_r="";
        fixedvtkfilename_r.append(buffer_r2);

	  }
	  
	char buffer[255];
	sprintf(buffer,"%s_%04d.mha",outputprefixVF.c_str(),step);
	outfilename="";
	outfilename.append(buffer);
	printf("Timestep %d\n",step);
	printf("Fixed    %s\n",fixedvtkfilename_l.c_str());
    printf("Fixed    %s\n",fixedvtkfilename_r.c_str());
    printf("Moving   %s\n",movingvtkfilename_l.c_str());
    printf("Moving   %s\n",movingvtkfilename_r.c_str());
	printf("Output   %s\n",outfilename.c_str());
    
    
	vtkSmartPointer<vtkPolyDataReader> fixedreader_l=vtkSmartPointer<vtkPolyDataReader>::New();
	fixedreader_l->SetFileName(fixedvtkfilename_l.c_str());
	fixedreader_l->Update();

	vtkSmartPointer<vtkPolyDataReader> movingreader_l=vtkSmartPointer<vtkPolyDataReader>::New();
	movingreader_l->SetFileName(movingvtkfilename_l.c_str());
	movingreader_l->Update();
        
    vtkSmartPointer<vtkPolyDataReader> fixedreader_r=vtkSmartPointer<vtkPolyDataReader>::New();
    fixedreader_r->SetFileName(fixedvtkfilename_r.c_str());
    fixedreader_r->Update();

    vtkSmartPointer<vtkPolyDataReader> movingreader_r=vtkSmartPointer<vtkPolyDataReader>::New();
    movingreader_r->SetFileName(movingvtkfilename_r.c_str());
    movingreader_r->Update();
    
	if (fixedreader_l->GetOutput()->GetNumberOfPoints() != movingreader_l->GetOutput()->GetNumberOfPoints())
	{
	  cout << "Lungs Left: Fixed and moving points must be the same number of points!" << endl;
	  exit(1);
	}
	
    if (fixedreader_r->GetOutput()->GetNumberOfPoints() != movingreader_r->GetOutput()->GetNumberOfPoints())
    {
      cout << "Lungs Right: Fixed and moving points must be the same number of points!" << endl;
      exit(1);
    }
    

	double *roibounds=new double[6];
	for (int i=0;i<3;++i)
	{
	  roibounds[2*i]=extent[2*i]*itkreader->GetOutput()->GetSpacing()[i]+itkreader->GetOutput()->GetOrigin()[i];
	  roibounds[2*i+1]=extent[2*i+1]*itkreader->GetOutput()->GetSpacing()[i]+itkreader->GetOutput()->GetOrigin()[i];
	}
	long int pointid=0;
	PointSetType :: Pointer pointSet = PointSetType :: New ();
	PointSetType :: PointType point;
	VectorType V;



	cout << "Creating set of known motion vectors..." << endl;

	// create bounding box zero motion points
	int dx=20;
	int *roiedge=new int[3];


	for (roiedge[0]=0;roiedge[0]<=dx;++roiedge[0])
	{
	  for (roiedge[1]=0;roiedge[1]<=dx;++roiedge[1])
	  {
	    for (roiedge[2]=0;roiedge[2]<=dx;++roiedge[2])
	    {
	      if (roiedge[0]==0 || roiedge[0]==dx || roiedge[1]==0 || roiedge[1]==dx || roiedge[2]==0 || roiedge[2]==dx) // do create zero motion vectors only on the surface of the roi box
	      {
		for (int i=0;i<3;++i)
		{
		  point[i]=roibounds[2*i]+roiedge[i]*(roibounds[2*i+1]-roibounds[2*i])/dx;
		}
		V[0]=0;
		V[1]=0;
		V[2]=0;
		pointSet -> SetPoint ( pointid , point );
		pointSet -> SetPointData ( pointid , V );
		++pointid;
	      }
	    }
	  }
	}


	for (int i=0;i<fixedreader_l->GetOutput()->GetNumberOfPoints();++i)
	{
	  double *fixpoint=fixedreader_l->GetOutput()->GetPoint(i);
	  double *movpoint=movingreader_l->GetOutput()->GetPoint(i);

	  V[0]=movpoint[0]-fixpoint[0];
	  V[1]=movpoint[1]-fixpoint[1];
	  V[2]=movpoint[2]-fixpoint[2];
	  point[0]=fixpoint[0];
	  point[1]=fixpoint[1];
	  point[2]=fixpoint[2];

	  pointSet -> SetPoint ( pointid , point );
	  pointSet -> SetPointData ( pointid , V );
	  ++pointid;
	}

	
    for (int i=0;i<fixedreader_r->GetOutput()->GetNumberOfPoints();++i)
    {
      double *fixpoint=fixedreader_r->GetOutput()->GetPoint(i);
      double *movpoint=movingreader_r->GetOutput()->GetPoint(i);

      V[0]=movpoint[0]-fixpoint[0];
      V[1]=movpoint[1]-fixpoint[1];
      V[2]=movpoint[2]-fixpoint[2];
      point[0]=fixpoint[0];
      point[1]=fixpoint[1];
      point[2]=fixpoint[2];

      pointSet -> SetPoint ( pointid , point );
      pointSet -> SetPointData ( pointid , V );
      ++pointid;
    }
    
    
  	cout << "Interpolating with B-Spline between known motion vectors..." << endl;

	VectorImageType::SizeType size;
	size[0]=extent[1]-extent[0]+1;
	size[1]=extent[3]-extent[2]+1;
	size[2]=extent[5]-extent[4]+1;
//	cout << "1" << endl;

	VectorImageType::PointType origin;
	origin[0]=roibounds[0];
	origin[1]=roibounds[2];
	origin[2]=roibounds[4];
//	cout << "2" << endl;
	
	VectorImageType::SpacingType spacing;
	spacing[0]=(roibounds[1]-roibounds[0])/(size[0]-1);
	spacing[1]=(roibounds[3]-roibounds[2])/(size[1]-1);
	spacing[2]=(roibounds[5]-roibounds[4])/(size[2]-1);
//	cout << "3" << endl;

	FilterType :: Pointer filter = FilterType :: New ();

	int no_splineorder=3;
	int no_cps=5;
	int no_levels=6;
//	int no_splineorder=1;
//	int no_cps=2;
//	int no_levels=3;
//	cout << "4" << endl;

	filter -> SetSize ( size );
	filter -> SetOrigin ( origin );
	filter -> SetSpacing ( spacing );
	filter -> SetInput ( pointSet );
	filter -> SetSplineOrder ( no_splineorder );
	FilterType :: ArrayType ncps ;
	ncps . Fill ( no_cps );
	filter -> SetNumberOfControlPoints ( ncps );
	filter -> SetNumberOfLevels ( no_levels );
//	cout << "5" << endl;
//	cout << filter << endl;
	try
	  {
	  filter -> Update ();
	  }
	catch (itk::ExceptionObject &err)
	  {
	  std :: cerr << "Test 2: itkBSplineScatteredDataImageFilter exception thrown " << std :: endl ;
	  std::cout << err << std::endl;
	  exit (1);
	  }

	cout << "Saving output vector field..." << endl;

	ImageMaskReaderType::Pointer nonmovingreader=ImageMaskReaderType::New();
	if (argc==16)
	{
	cout << "1" << endl;
	nonmovingreader->SetFileName(nonmovingpointsfilename.c_str());
	nonmovingreader->Update();
	}
	cout << "2" << endl;

	VectorImageType::Pointer outimage=VectorImageType::New();
	outimage->SetRegions(itkreader->GetOutput()->GetLargestPossibleRegion());
	outimage->SetSpacing(itkreader->GetOutput()->GetSpacing());
	outimage->SetOrigin(itkreader->GetOutput()->GetOrigin());
	outimage->Allocate();
	VectorImageType::PixelType vectornomotion;
	vectornomotion.Fill(0);
	outimage->FillBuffer(vectornomotion);

	cout << "3" << endl;

	for (unsigned int x=0;x<size[0];++x)
	{
	  for (unsigned int y=0;y<size[1];++y)
	    {
	      for (unsigned int z=0;z<size[2];++z)
		{
		  VectorImageType::IndexType filterindex;
		  filterindex[0]=x;
		  filterindex[1]=y;
		  filterindex[2]=z;
		  VectorImageType::PixelType vector;
		  vector=filter->GetOutput()->GetPixel(filterindex);
		  VectorImageType::IndexType outputindex;
		  outputindex[0]=filterindex[0]+extent[0];
		  outputindex[1]=filterindex[1]+extent[2];
		  outputindex[2]=filterindex[2]+extent[4];
		  if (outimage->GetLargestPossibleRegion().IsInside(outputindex)) {
              VectorImageType::IndexType pointing_to_index;
              for (int j=0;j<3;++j)
              {
                pointing_to_index[j]=outputindex[j]+static_cast<int>(round(vector[j]/spacing[j]));
              }
              if (argc==16)
              {
                if (nonmovingreader->GetOutput()->GetPixel(outputindex)==0){
                    
                    if (outimage->GetLargestPossibleRegion().IsInside(pointing_to_index) && nonmovingreader->GetOutput()->GetPixel(pointing_to_index)==0)
                    {
                    outimage->SetPixel(outputindex,vector);
                    }
                    else if (! outimage->GetLargestPossibleRegion().IsInside(pointing_to_index) || nonmovingreader->GetOutput()->GetPixel(pointing_to_index)!=0) 
                    {
                    double fraction_of_vector=0.95;
                    while (fraction_of_vector>0) {
                    VectorImageType::PixelType newvector;
                    for (int j=0;j<3;++j)
                    {
                    newvector[j]=vector[j]*fraction_of_vector;
                    pointing_to_index[j]=outputindex[j]+static_cast<int>(round(newvector[j]/spacing[j]));
                    }
                    if (outimage->GetLargestPossibleRegion().IsInside(pointing_to_index) && nonmovingreader->GetOutput()->GetPixel(pointing_to_index)==0)
                    {
                    // the vector should not point to close to the non moving area, so pull it a voxel back
                    double fact_of_vector_for_half_voxel=sqrt(spacing[0]*spacing[0]+spacing[1]*spacing[1]+spacing[2]*spacing[2])/sqrt(vector[0]*vector[0]+vector[1]*vector[1]+vector[2]*vector[2]);
                    if (fact_of_vector_for_half_voxel<1) {
                        for (int j=0;j<3;++j) {
                        newvector[j]=(1-fact_of_vector_for_half_voxel)*newvector[j];
                        }
                        outimage->SetPixel(outputindex,newvector);
                    }
                    break;
                    }
                    fraction_of_vector-=0.05;
                    }
                    }
              }            
              }
              else
              {
                outimage->SetPixel(outputindex,vector);
              }
		  }
		}
	    }
	}

	VectorImageWriterType::Pointer outwriter=VectorImageWriterType::New();
	outwriter->SetInput(outimage);
	outwriter->SetUseCompression(true);
	outwriter->SetFileName(outfilename.c_str());
	outwriter->Update();
	outwriter->Write();

	if (vectorfield_or_deffield==1)
	  {
	    cout << "Writing warped timestep output." << endl;
	    string warpedfilename;
	    char buffer[255];
	    sprintf(buffer,"%s_%04d.mha",outputprefixWarpedImage.c_str(),step);
	    warpedfilename.append(buffer);

	    WarperType::Pointer warper = WarperType::New();
	    InterpolatorType::Pointer interpolator = InterpolatorType::New();

	    warper->SetInput( itkreader->GetOutput() );
	    warper->SetInterpolator( interpolator );
	    warper->SetOutputSpacing( itkreader->GetOutput()->GetSpacing() );
	    warper->SetOutputOrigin( itkreader->GetOutput()->GetOrigin() );
	    warper->SetDisplacementField( outimage );

	    ImageWriterType::Pointer warpedwriter = ImageWriterType::New();
	    warpedwriter->SetInput(warper->GetOutput());
	    warpedwriter->SetFileName(warpedfilename.c_str());
	    warpedwriter->Update();
	    warpedwriter->Write();
	  }

	} // end for each timestep
	cout << "done !" << endl;
}
