clear all; clc;close all;
addpath('../matlab-scripts');
addpath('functions');

%Script to create moving MR meshes
%Created by Alisha Duetschler June 2021

% Prequisites: 
%   -MRI lung surface meshes
%   -MRI deformation vector fields

% Procedure:
%   1) Insert grid points inside MRI meshes (insertGridPointsInsideMRmesh.m)
%   2) Extract motion vectors at MRI mesh points and apply to mesh (readMotionvectorsforMesh.m)

%% 

%% input that has to be changed
MRINo = 1;

MRIbasepath = '../../example/data/MRI/MRI';

start_phase = 24; 
end_phase = 45; %two cycles in this example

%% define paths
MR_path = [MRIbasepath num2str(MRINo) '/']
MR_mesh_basename = [MR_path '/structures/lungs_']
DVF_path=[MR_path 'dvfs/']; %MRI DVFs
MRI=[MR_path 'MRI.mha'];

%% set up directories
MovingMR_mesh_basepath = [MR_path '/MovingMRmesh/'];
str = ['mkdir -p ' MovingMR_mesh_basepath];
system(str);


%% for both sides separately first insert points inside of the mesh and then apply the DFVs to the meshes
for sides = 0:1

    if sides==0
        side = 'Left'
    else
        side = 'Right'
    end
    
    %make directories for meshes
    str = ['mkdir -p ' MovingMR_mesh_basepath side];
    system(str);


    %%
    disp('---------------------------------------------------');
    disp(' Insert grid points inside MR meshes');
    % based on ray casting
    insertGridPointsInsideMRmesh(MR_mesh_basename, MovingMR_mesh_basepath, side);
  

    %%
    disp('---------------------------------------------------');


    disp(' Extract motion vectors at MRI mesh points and apply to CT mesh');

    for phase=start_phase:end_phase
        applyMotionToMRmesh(MovingMR_mesh_basepath, DVF_path, MRI, side, phase);
    end

    disp('---------------------------------------------------');

end


disp('---------------------------------------------------');



