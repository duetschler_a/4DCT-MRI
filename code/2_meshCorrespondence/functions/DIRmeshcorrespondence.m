function DIRmeshcorrespondence(fixedMHA,movingMHA,fixedSurfaceMesh,useLandmarks,fixedLandmarks,movingLandmarks,outputFolder,side)

%% plastimatch DIR
disp('------------------------------------------------------------------------------');
disp(['run deformable image registration ' side]);

preparePlastimatchDIRfiles(fixedMHA,movingMHA,useLandmarks,fixedLandmarks,movingLandmarks,outputFolder,side)

plastimatch_File = [outputFolder 'plastimatch_' side '.txt'];
runplastimatch=['plastimatch register ' plastimatch_File];
system(runplastimatch);


% %% calculate dice coefficients (original files and after DIR)
% disp('------------------------------------------------------------------------------');
% disp('calculate Dice coefficients and Hausdorff distances');
% dicestr=['echo BEFORE DIR > ' outputFolder 'dice_' side '.txt']; 
% system(dicestr);
% dicestr=['plastimatch dice --all ' fixedMHA ' ' movingMHA ' >> ' outputFolder 'dice_' side '.txt'];
% system(dicestr);
% dicestr=['echo ---------------------------- >> '  outputFolder 'dice_' side '.txt']; 
% system(dicestr);
% dicestr=['echo AFTER DIR >> ' outputFolder 'dice_' side '.txt']; 
% system(dicestr);
% dicestr=['plastimatch dice --all ' fixedMHA ' ' warpedMHA ' >> ' outputFolder 'dice_' side '.txt'];
% system(dicestr);

%% warp surface mesh
disp('------------------------------------------------------------------------------');
disp(['deform surface mesh ' side]);

deformedSurfaceMesh = [outputFolder 'deformed_lungs_' side '.vtk'];
dvf = [outputFolder 'vf_' side '.mha'];
warpSurfaceMesh(fixedSurfaceMesh,dvf,deformedSurfaceMesh)







