clear;clc;close all;
addpath('../matlab-scripts/');
addpath('functions');

%Script to prepare plastimatch files to establish anatomical lung correpsondence using deformable image registration (DIR)
%Created by Alisha Duetschler August 2019
%Adapted in June 2021

%Prequisites: 
%   -smooth lung masks for CT and MRI (/structures/lungs_left_smooth.mha
%   and /structures/lungs_right_smooth.mha)
%   -plastimatch installation

%   1) plastimatch B-spline registration between MRI and CT lung masks (for
%   each side separately) (DVF:MRI ---> CT )
%   2) In terminal execute generated scripts (in
%   example/results/MRI<i>_CT<j> folder) using 'plastimatch register plastimatch_left.txt' and 
%   'plastimatch register plastimatch_right.txt'


%% input that has to be changed
CTNo = 1;
MRINo = 1;

CTbasepath = '../../example/data/CT/CT';
MRIbasepath = '../../example/data/MRI/MRI';
outputbasepath = '../../example/results/';


% enter landmark location if needed to guide registration
useLandmarks = false;
fixedLandmarks = '';
movingLandmarks = '';

% useLandmarks = true;
% if using landmarks to guide the registration save them in the follwing
% location und with the name lms_<side>_MRI.csv and lms_<side>_CT.csv
% fixedLandmarks = '../../example/results/MRI1_CT1/lms_right_MRI.csv';
% movingLandmarks = '../../example/results/MRI1_CT1/lms_right_CT.csv';

%% 
% perform for each side separately
for s = 1:2
    if s ==1
        side = 'left';
    else
        side = 'right';
    end
    
    if useLandmarks == true
        fixedLandmarks = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/lms_' side '_MRI.csv'];
        movingLandmarks = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/lms_' side '_CT.csv'];
    end
        
    
    fixedMHA = [MRIbasepath num2str(MRINo) '/structures/lungs_' side '_smooth.mha']
    movingMHA = [CTbasepath num2str(CTNo) '/structures/lungs_' side '_smooth.mha']
 
    outputFolder = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/']

    %% make output directory 

    str=['mkdir -p ' outputFolder];
    system(str);

    %% prepare files for plastimatch DIR 
    
    preparePlastimatchDIRfiles(fixedMHA,movingMHA,useLandmarks,fixedLandmarks,movingLandmarks,outputFolder,side)
end



