clear;clc;close all;
addpath('../matlab-scripts/');
addpath('functions');

%Deform MRI surface lung meshes accoring to deformation vector fields form
%mask regirstration to establish anatomical correspondence
%Created by Alisha Duetschler August 2019
%Adapted in June 2021

%Prequisites: 
%   -CT and MRI lung surface meshes (/structures/lungs_left.vkt and
%   /structures/lungs_right.vtk)
%   -results of image registration (vf_left.mha and vf_right.mha in
%   example/results/MRI<i>_CT<j> folder)


%% input that has to be changed
CTNo = 1;
MRINo = 1;

CTbasepath = '../../example/data/CT/CT';
MRIbasepath = '../../example/data/MRI/MRI';
outputbasepath = '../../example/results/';

%% 
% perform for each side separately
for s = 1:2
    if s ==1
        side = 'left';
    else
        side = 'right';
    end
    outputFolder = [outputbasepath 'MRI' num2str(MRINo) '_CT' num2str(CTNo) '/'];
        
    fixedSurfaceMesh = [MRIbasepath num2str(MRINo) '/structures/lungs_' side '.vtk'];
    movingSurfaceMesh = [CTbasepath num2str(CTNo) '/structures/lungs_' side '.vtk'];
    deformedSurfaceMesh = [outputFolder 'deformed_lungs_' side '.vtk'];

    %% copy lung meshes
    str = ['cp ' fixedSurfaceMesh ' ' outputFolder 'fixed_lungs_' side '.vtk'];
    system(str);
    str = ['cp ' movingSurfaceMesh ' ' outputFolder 'moving_lungs_' side '.vtk'];
    system(str);

    %% deform mesh
    disp(['deform surface mesh ' side]);
    dvf = [outputFolder 'vf_' side '.mha'];
    warpSurfaceMesh(fixedSurfaceMesh,dvf,deformedSurfaceMesh)
end



