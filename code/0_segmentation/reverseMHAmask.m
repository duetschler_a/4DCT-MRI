addpath('../matlab-scripts');

filename = '../../example/data/CT/CT1/structures/NonmovingMask_Body_Rev.mha';

%%

[nonMovingMaskRev header]=read_MHA(filename);

reversedMask = (nonMovingMaskRev == 0) * 255;

% Write reversed mask
writemha([filename(1:end-8) '.mha'],reversedMask,header.origin,header.spacing,'float');
