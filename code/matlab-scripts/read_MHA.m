function [ image_data image_info ] = read_MHA( filename, verbose)
%READMHA Summary of this function goes here
%   Detailed explanation goes here
if nargin < 2, verbose=0; end
if (verbose)
    disp(sprintf('Reading file %s ...\n', filename));
end

header=mha_read_header(filename);

image_info.dimensions=header.Dimensions;
image_info.spacing=header.PixelDimensions;
image_info.origin=header.Offset;

image_data=double(mha_read_volume(header));

end

