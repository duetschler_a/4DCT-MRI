# 4DCT(MRI)

**Code for creating synthetic 4DCT(MRI)s from a reference CT and a 4DMRI**

----------------------------------------------------------------------------------------------------

Please cite the following publications when using the code:

Duetschler, A., Bauman, G., Bieri, O., Cattin, P.C., Ehrbar, S., Engin-Deniz, G., Giger, A., Josipovic, M., Jud, C., Krieger, M., Nguyen, D., Persson, G.F., Salomir, R., Weber, D.C., Lomax, A.J. and Zhang, Y. (2022), *Synthetic 4DCT(MRI) lung phantom generation for 4D radiotherapy and image guidance investigations*. Med. Phys.. [https://doi.org/10.1002/mp.15591](https://doi.org/10.1002/mp.15591)

Duetschler A, Krieger M, Lomax AJ, Zhang Ye (2021). 4DCT(MRI). Zenodo. [doi:10.5281/zenodo.5010964](https://doi.org/10.5281/zenodo.5010964)

In case of questions, please contact Alisha Dütschler (alisha.duetschler@psi.ch).

----------------------------------------------------------------------------------------------------

**4DMRI lung mesh data**

Additional 4DMRI data in the form of moving lung meshes suitable for the generation of 4DCT(MRI)s can be found [here](https://gitlab.psi.ch/duetschler_a/4DMRI_moving_lung_meshes).


![Workflow](4D-CT(MRI)workflow.png)

----------------------------------------------------------------------------------------------------
## Setup instructions and prerequisites

Most of the code is written in MATLAB. There are, however, also parts written in java and C++. In this case, we provide the executables for direct use as well as the source code.

**Operating system**

The code was developed and tested on Linux. The java and C++ executables will not work on other operating systems.

**1. Clone repository**

You can clone the repository from Gitlab. In the terminal navigate to the desired location and clone the repository

    git clone https://gitlab.psi.ch/duetschler_a/4DCT-MRI.git
    
**2. Download plastimatch**

The open-source software [Plastimatch](http://plastimatch.org/) is used for image registration. Please follow the download instructions on the website. The use of other image registration software is also possible.
Plastimatch can also be used to convert dicom data to mha data and back.

**3. Other useful tools**

You might also consider to download [Slicer](https://www.slicer.org/) (for image segmentation and landmark definition), the [Scalismo Ui](https://github.com/unibas-gravis/scalismo-ui) (for landmark definition), [ParaView](https://www.paraview.org/) (for mesh visualization) and [vv](https://www.creatis.insa-lyon.fr/rio/vv/) (for mha visualization).

We provide executables for the java code, however, the source code is also provided and you would require a java setup (e.g. in eclipse) and vtk libraries for further developments of these codes. The C++ code relies on vtk and itk.


----------------------------------------------------------------------------------------------------
## Documentation

We suggest you follow the naming conventions for the necessary files suggested below, otherwise further changes to the code at different levels might be needed.

**Prerequisites**

- reference CT in mha format (called CT.mha) in a separate folder (referred to as CTfolder from now on)
- 4DMRI (or 4DCT)
    - reference MRI state in mha format (called MRI.mha) in a separate folder (referred to as MRIfolder from now on)
    - moving MRI meshes (MRIfolder/MovingMRmesh) for both sides for the different states
    - or deformation vector fields of each 4DMRI state registered to the reference MRI state (dvf_0000.mha, dvf_0001.mha etc in folder MRIfolder/dvfs)
    
Notes: 
- The same reference breathing state should be chosen for the CT and MRI (i.e. end exhale for both or DIBH CT and an end inhalation MRI reference state).
- Make sure the CT and 4DMRI have the same orientation.
- The 4DMRI registration can for example be performed using the open-source [Plastimatch](http://plastimatch.org/) software.

**O. Lung segmentation**

* Procedure:
* [ ]  The first step is the segmentation of both lung halves (separately). This can for example be done using the open-source software [Slicer](https://www.slicer.org/) or [itkSnap](http://www.itksnap.org/). The segmentation results should be saved in the folders CTfolder/structures and MRIfolder/structures as lungs_left.mha and lungs_right.mha. The value 255 has to be assigned to the lung halves and 0 otherwise.

* [ ]  Additionally a NonmovingMask_Body.mha in the folder CTfolder/structures will also be needed. The value 255 is assigned to the non-moving parts (outside, ribcage, body-surface, ...), whereas the moving parts (inside of ribcage: lungs, heart, liver, ...) is marked by the value zero (see example mask). If you prefer to use the lung masks and extend them into the liver, heart, etc. and reverse the resulting mask please use the reverseMHAmask.mha script in the folder 4DCT-MRI/code/0_segmentation. 


* Output:
    * lungs_left.mha, lungs_right.mha and NonmovingMask_Body.mha in the folder CTfolder/structures
    * lungs_left.mha and lungs_right.mha in the folder MRIfolder/structures

**1. Reference mesh generation**

The next step is the generation of lung meshes for the CT and the reference MRI. This is done using vtk in java. The java source code can be found in 4DCT-MRI/code/1_meshGeneration together with the executables.

If you use the provided volunteer 4DMRI motion data, the meshes are already provided and you can skip the generation of the MRI mesh.

* TOOL: **BinaryImageToMesh**

* Procedure/input:
* [ ] navigate to the 1_meshGeneration/BinaryImageToMesh folder
* [ ] ./BinaryImageToMesh CTfolder/structures/lungs_left.mha CTfolder/structures/lungs_left.vtk 
* [ ] ./BinaryImageToMesh CTfolder/structures/lungs_right.mha CTfolder/structures/lungs_right.vtk
* repeat for  MRIfolder if you want to use your own motion data

* Output:
    * meshes (lungs_left.vtk, lungs_right.vtk) and smoothed masks (lungs_left_smooth.mha, lungs_right_smooth.mha) in the folder CTfolder/structures
    * (meshes (lungs_left.vtk, lungs_right.vtk) and smoothed masks (lungs_left_smooth.mha, lungs_right_smooth.mha) in the folder MRIfolder/structures)

Technically, only the lung meshes for the reference MRI are used and not for the CT, however, the CT lung meshes can be used to compare the result of the registration and the warping of the MRI mesh. 

Additionally, the code also saves a lungs_left_smooth.mha and lungs_right_smooth.mha for the CT and MRI. This is obtained from the original mask through a downsampling to 5x5x5 mm³ and a Gaussian smoothing and will be used in the next step (alternatively you can also use the SmoothBinaryImage function for this). The smoothing and resampling is not necessary but usually lead to better registration results. 

If you prefer to use another method to generate triangle meshes, please make sure your meshes don't have too many mesh points as this could lead to memory issues or long computation times.

There is also a script to generate binary mha files from vtk surface meshes (MeshToBinaryImage) which can be useful.

**2. Establish mesh correspondence through the registration of binary masks**

Correspondence is established through a plastimatch registration of the smoothed lung masks of the reference MRI and the CT for both sides separately. The results of the registration are used to warp the MRI surface mesh into a corresponding mesh for the CT.

These steps are all performed in the callDIRmeshcorrespondence.m script. After setting up the appropriate paths, the function DIRmeshcorrespondence is called, which adapts the plastimatch template command file (plastimatch_align_center_affine_bspline_template.txt) and performs the registration and then uses the deformation vector field to warp the CT mesh. The registration process is done separately for each side.

* TOOL: **callDIRmeshcorrespondence.m**

* Procedure:
* [ ] Adapt CT and MRI numbers and paths. 
* [ ] Optional: rerun with landmarks by setting useLandmarks = true and entering the path to both landmark files ¹.

* Output: for each side (left/right)    
    * plastimatch_side.txt command file
    * warped_mask_side.mha: result of registration (should be similar to MRI mask)
    * vf_side.mha: deformation vector field from registration
    * moving_lungs_side.vtk and fixed_lungs_side.vtk: copy of CT and MRI lung mesh
    * deformed_lungs_side.vtk: new corresponding CT lungs obtained by applying the vf_side.mha to points of fixed_lungs_side.vtk
    
Suggested workflow: Try a registration (for both sides) without landmarks first and deform the CT mesh by executing the callDIRmeshcorrespondence.m script. Load the resulting meshes deformed_lungs_left.vtk and deformed_lungs_right.vtk into ParaView and compare them to the original moving_lungs_left.vtk and moving_lungs_right.vtk of the reference CT. The correspondence can also be checked in ParaView by comparison to fixed_lungs_left.vtk and fixed_lungs_right.vtk of the reference MRI.
If parts are missing or are too large, you can define one or more landmarks in these locations and redo the previous steps using the landmarks to guide the registration.  

Note:
¹ **Landmarks** can be defined by loading meshes in the [Scalismo Ui](https://github.com/unibas-gravis/scalismo-ui) and clicking landmarks either on slices or on 3D meshes in the same location for the CT and the MRI. After saving the landmarks in a csv file, the sign of the first two coordinates has to be flipped ((x,y,z)-->(-x,-y,z)).
Alternatively, [Slicer](https://www.slicer.org/) can also be used to define landmarks, but only on slices not on the 3D mesh. Also, save the landmarks in a csv file. Slicer already uses the correct coordinate system for Plastimatch. 
Make sure to use the same names in both landmark files for corresponding landmarks and use the same order.

**3. Insert points inside meshes, apply motion to meshes and warp reference CT**

If you want to use your own motion data you first have to create moving meshes. Each state of the 4DMRI has to be registered to the reference MRI state and the resulting deformation vector fields are used to warp the reference lung mesh. You can use the **createMovingMRmeshes.m** script for this. If you use the provided 4DMRI motion data the moving meshes are already provided and you can skip this step.

The following script combines all the remaining steps needed in order to generate a new 4DCT(MRI). The steps involved are described below. 
* TOOL: **grid_motion_warp.m**

* Procedure:
* [ ] Adapt CT and or MRI numbers and paths. 
* [ ] Adapt the reference CT phase and the start and end phase for the 4DMRI. 
* [ ] Run MATLAB script.

* Steps:
    * insertGridPointsInsideCTmesh.m: for both sides insert corresponding points also inside the meshes. This relies on a open-source MATLAB triangle/ray intersection implementation [1].
        * Output: lungs_left_grid.vkt, lungs_right_grid.vtk, lungs_left_surface_and_grid.vtk and lungs_right_surface_and_grid.vtk in folder MovingCTmesh.
    * applyMotionToCTmesh.m: applies motion of MR mesh points to corresponding CT mesh points resulting in a CT mesh for each state of the 4DMRI.
        * Output: folder MovingCTmesh/Side with a state_000X.mha mesh for each state X.
    * create_simulated_vectorfield_bothSides: executable from C++ code. Warping of the reference CT based on interpolated motion of CT meshes using the non-moving mask.
        * Output: mha files of the states of the 4DCT-MRI in the folder MovingCT and motionfields used to warp the reference CT and inverse deformationfields in folder MotionVectors. 

----------------------------------------------------------------------------------------------------
## Example data

The provided example CT was modified from the The Cancer Image Archive [2] 4D-Lung dataset [3-6] licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/). The CT is from patient 115 (50% phase, end exhale), which was converted from dicom to mha format. Each lung half was manually contoured on the reference phase.

The provided 4DMRI lung mesh data are from MRI1 and more data can be found [here](https://gitlab.psi.ch/duetschler_a/4DMRI_moving_lung_meshes).

----------------------------------------------------------------------------------------------------
## References


[1] Jaroslaw Tuszynski (2021). Triangle/Ray Intersection (https://www.mathworks.com/matlabcentral/fileexchange/33073-triangle-ray-intersection), MATLAB Central File Exchange. Retrieved June 11, 2021. 

[2] Clark K, Vendt B, Smith K, Freymann J, Kirby J, Koppel P, Moore S, Phillips S, Maffitt D, Pringle M, Tarbox L, Prior F. The Cancer Imaging Archive (TCIA): maintaining and operating a public information repository. Journal of Digital Imaging. 2013 Dec;26(6):1045-57. DOI: 10.1007/s10278-013-9622-7

[3] Hugo, Geoffrey D., Weiss, Elisabeth, Sleeman, William C., Balik, Salim, Keall, Paul J., Lu, Jun, & Williamson, Jeffrey F. (2016). Data from 4D Lung Imaging of NSCLC Patients. The Cancer Imaging Archive. http://doi.org/10.7937/K9/TCIA.2016.ELN8YGLE

[4] Hugo, G. D., Weiss, E., Sleeman, W. C., Balik, S., Keall, P. J., Lu, J. and Williamson, J. F. (2017), A longitudinal four-dimensional computed tomography and cone beam computed tomography dataset for image-guided radiation therapy research in lung cancer. Med. Phys., 44: 762–771. doi:10.1002/mp.12059

[5] S. Balik et al., “Evaluation of 4-Dimensional Computed Tomography to 4-Dimensional Cone-Beam Computed Tomography Deformable Image Registration for Lung Cancer Adaptive Radiation Therapy.” Int. J. Radiat. Oncol. Biol. Phys. 86, 372–9 (2013) PMCID: PMC3647023.

[6] N.O. Roman, W. Shepherd, N. Mukhopadhyay, G.D. Hugo, and E. Weiss, “Interfractional Positional Variability of Fiducial Markers and Primary Tumors in Locally Advanced Non-Small-Cell Lung Cancer during Audiovisual Biofeedback Radiotherapy.” Int. J. Radiat. Oncol. Biol. Phys. 83, 1566–72 (2012). DOI:10.1016/j.ijrobp.2011.10.051
